// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    doLoadGroups();
    setUpHeaderBar();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLogin() {
    let usernameElement = document.querySelector('#loginUsername');
    let passwordElement = document.querySelector('#loginPassword');
    let credentials = {
        username: usernameElement.value,
        password: passwordElement.value
    };
    let loginResponse = await postCredentials(credentials);
    if (loginResponse.id > 0) {
        localStorage.setItem('LOGIN_USERNAME', loginResponse.username);
        localStorage.setItem('LOGIN_TOKEN', loginResponse.token);
        setUpHeaderBar();
        closePopup();
        doLoadGroups();
    } else {
        // Ilmnes mingi jama, lahendame selle siin...
        doLogout();
        displayLoginPopup();
    }
}

function doLogout() {
    localStorage.removeItem('LOGIN_USERNAME');
    localStorage.removeItem('LOGIN_TOKEN');
    displayLoginPopup();
}

async function doLoadGroups() {
    console.log('Loading Groups...');
    let groups = await fetchGroups();
    if (groups) {
        displayGroups(groups);
    } else {
        displayLoginPopup();
    }
}

async function doDeleteGroup(groupId) {
    if (confirm('Do you really wish to delete this group?')) {
        await removeGroup(groupId);
        await doLoadGroups();
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayGroups(groups) {
    const mainElement = document.querySelector('#groupsList');
    let groupsHtml = '';

    for (const group of groups) {
        groupsHtml += /*html*/`
            <div class="group-item-box">
                <div class="group-item-property">Nimi: </div>
                <div class="group-item-value">${group.group_name}</div>
                <div class="group-item-property">Kuutasu: </div>
                <div class="group-item-value">${group.monthly_rate}</div>
                <div class="group-item-button">
                    <button class="btn btn-danger onclick="doDeleteGroup(${group.id})">Kustuta</button>
                    <button class="btn btn-secondary" onclick="displayGroupEditPopup(${group.id})">Muuda</button>
                    <button class="btn btn-secondary" onclick="displayNewTrainingPopup(${group.id})">Lisa treening</button>
                </div>
            </div>
        `;
    }

    mainElement.innerHTML = groupsHtml;
}



function displayMembersList(members) {
    let membersHtml = '';

    for (const member of members) {
        membersHtml += /*html*/`
            <div class="container">
                <input type="checkbox" value="${member.id}" id="cb_${member.id}" name="participatingMembers">
                <label for="cb_${member.id}" class="company-item-value">${member.first_name} ${member.last_name}</label>                
            </div>
        `;
    }
    return membersHtml;
}


// function displayAnnouncements(announcements) {
    // let newsHtml = '';

    // for (let announcement of announcements) {
    //     newsHtml += /*html*/`
    //         <div>
    //             <h4>${announcement.title}</h4>
    //             <p>${announcement.body}</p>
    //         </div>
    //     `;
    // }

//     return newsHtml;
// }

async function displayLoginPopup(){
    openPopup(POPUP_CONF_BLANK_300_300, 'loginTemplate');
}

async function displayGroupEditPopup(groupId) {
    await openPopup(POPUP_CONF_500_500, 'groupEditTemplate');

    if (groupId > 0) {
        // Muudame olemasolevat gruppi.
        // Täidame vormi väljad...
        const group = await fetchGroup(groupId);
        document.querySelector('#groupId').value = group.id;
        document.querySelector('#groupName').value = group.name;
        document.querySelector('#groupMonthlyFee').value = group.monthlyFee;
    }
}

async function displayNewTrainingPopup(groupId) {
    await openPopup(POPUP_CONF_500_500, 'newTrainingTemplate');
    if (groupId > 0) {
        //Lisame osalejad(members)
        const group = await fetchGroup(groupId);
        document.querySelector('#groupId').value = group.id;
        document.querySelector('#groupName').textContent = group.group_name;
        console.log(group.members);
        document.querySelector('#membersDiv').innerHTML = displayMembersList(group.members);
        
    }
}



async function editGroup() {
    const validationResult = validateGroupForm();

    if (validationResult.length == 0) {
        let group;
        if (document.querySelector('#groupId').value > 0) {
            // Olemasoleva grupi muutmine.
            group = {
                id: document.querySelector('#groupId').value,
                name: document.querySelector('#groupName').value,
                monthlyFee: document.querySelector('#groupMonthlyFee').value
            };
            await putGroup(group);
        } else {
            // Uue grupi lisamine.
            group = {
                name: document.querySelector('#groupName').value,
                monthlyFee: document.querySelector('#groupMonthlyFee').value
            };
            await postGroup(group);
        }
        await doLoadGroups();
        await closePopup();
    } else {
        displayGroupFormErrors(validationResult);
    }
}


function validateGroupForm() {
    let errors = [];

    let groupName = document.querySelector('#groupName').value;
    let groupMonthlyFee = document.querySelector('#groupMonthlyFee').value;

    if (groupName.length < 2) {
        errors.push('Grupi nimi kas määramata või liiga lühike (min 2 tähemärki)!');
    }
    
    if (groupMonthlyFee < 1) {
        errors.push('Kuutasu peab olema vähemalt 1 EUR!');
    }

    return errors;
}

async function addNewTraining() {
    // const validationResult = validateNewTrainingForm();

    // if (validationResult.length == 0) {
        let training;
        // if (document.querySelector('#groupId').value > 0) {
        //     // Olemasoleva grupi muutmine.
            
        // } else {
            // Uue grupi lisamine.
            training = {
                group_id: document.querySelector('#groupId').value,
                training_date: document.querySelector('#trainingDate').value,
                comments: document.querySelector('#trainingComments').value,
                participatedMembers: getParticipatedMembers()
            };
            console.log(training);
            await postTraining(training);
        // }
        await doLoadGroups();
        await closePopup();
    // } else {
    //     displayGroupFormErrors(validationResult);
    // }
}

function getParticipatedMembers() {
    let participants = [];
    let participantCbs = document.querySelectorAll('input[name=participatingMembers]');

    for (let checkboxElement of participantCbs) {
        if (checkboxElement.checked) {
            participants.push(parseInt(checkboxElement.value));
        }
    }

    return participants;
}

// function validateNewTrainingForm() {
//     let errors = [];

//     let groupTrainingDate = document.querySelector('#groupTrainingDate').value;

//     if (groupTrainingDate.length == null) {
//         errors.push('Kuupäev märkimata!');
//     }
//     return errors;
// }


function displayGroupFormErrors(errors) {
    const errorBox = document.querySelector('#errorBox');
    errorBox.style.display = 'block';

    let errorsHtml = '';
    for (let errorMessage of errors) {
        errorsHtml += /*html*/`<div>${errorMessage}</div>`;
    }

    errorBox.innerHTML = errorsHtml;
}

function setUpHeaderBar() {
    document.querySelector('#headerBar span').textContent = localStorage.getItem('LOGIN_USERNAME');
}
