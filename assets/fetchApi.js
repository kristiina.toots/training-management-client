
// async function fetchGroups() {
//     let result = await fetch("http://localhost:8080/groups/all");
// }

async function postCredentials(credentials) {
    try {
        const response = await fetch(`${API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        });
        return await response.json();
    } catch (e) {
        console.log('LOGIN FAILED', e);
    }
}

async function fetchGroups() {
    try {
        const response = await fetch(`${API_URL}/groups/all`, {
            method: 'GET',
            // headers: {
            //     'Authorization': composeBearerToken()
            // }
        });

        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function fetchGroup(groupId) {
    try {
        const response = await fetch(`${API_URL}/groups/${groupId}`, {
            method: 'GET',
            headers: {
                //'Authorization': composeBearerToken()
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function removeGroup(groupId) {
    try {
        const response = await fetch(`${API_URL}/groups/${groupId}`, {
            method: 'DELETE',
            headers: {
                'Authorization': composeBearerToken()
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function postGroup(group) {
    try {
        const response = await fetch(`${API_URL}/groups/add`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': composeBearerToken()
            },
            body: JSON.stringify(group)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING GROUP', e);
    }
}


async function postTraining(training) {
    try {
        const response = await fetch(`${API_URL}/trainings/add`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': composeBearerToken()
            },
            body: JSON.stringify(training)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING TRAINING', e);
    }
}


async function putGroup(group) {
    try {
        const response = await fetch(`${API_URL}/groups/update`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': composeBearerToken()
            },
            body: JSON.stringify(group)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN MODIFYING GROUP', e);
    }
}

function composeBearerToken() {
    return `Bearer ${localStorage.getItem('LOGIN_TOKEN')}`;
}

async function processProtectedResponse(response) {
    if (response.status >= 400 && response.status <= 403) {
        throw new Error('Unauthorized');
    } else {
        return await response.json();
    }
}
